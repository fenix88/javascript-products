// Product Constructor
class Product {
    constructor(name, price, year) {
        this.name = name;
        this.price = price;
        this.year = year;
    }
}

// UI Constructor
// la clase UI es la que ba a acceder directamente al DOM para modificar el html
class UI {
    addProduct(product) {
        //accedo al div
        const productList = document.getElementById('product-list');
        // le entrego el product al div
        const element = document.createElement('div');
        //llenamos el div con html
        element.innerHTML = `
            <div class="card text-center mb-4">
                <div class="card-body">
                    <strong>Product</strong>: ${product.name} -
                    <strong>Price</strong>: ${product.price} - 
                    <strong>Year</strong>: ${product.year}
                    <a href="#" class="btn btn-danger" name="delete">Delete</a>
                </div>
            </div>
        `;
        // al div 'product-list' le entregamos los elementos
        productList.appendChild(element);
    }

    resetForm() {
        document.getElementById('product-form').reset();
    }

    deleteProduct(element) {
        //conprobamos que de un click en el enlace delete
        if (element.name === 'delete') {
            // con parentElement subimos de niveles para remover todos los div involucrados
            element.parentElement.parentElement.remove();
            this.showMessage('Product Deleted Succsssfully', 'success');
        }
    }

    // recibe como parametro el mensaje que quiero que se muestre 
    // y la clase de boostrap para el color del mensaje
    showMessage(message, cssClass) {
        const div = document.createElement('div');
        //diseñamos el div con estilo
        div.className = `alert alert-${cssClass} mt-2`;
        // le agregamos el mensaje al div
        div.appendChild(document.createTextNode(message));
        // Show in The DOM
        // selecciono todo el contenido del documento
        const container = document.querySelector('.container');
        // selecciono solo la app
        const app = document.querySelector('#App');
        // Insert Message in the UI,este div estara antes de mi aplicacion
        container.insertBefore(div, app);
        // Remove the Message after 3 seconds
        setTimeout(function () {
            document.querySelector('.alert').remove();
        }, 3000);
    }
}

// DOM Events
document.getElementById('product-form')
    .addEventListener('submit', function (e) {

        // captura los datos
        const name = document.getElementById('name').value,
            price = document.getElementById('price').value,
            year = document.getElementById('year').value;

        // Create a new Oject Product pasando los productos rescatados
        const product = new Product(name, price, year);

        // Create a new UI para ver los datos por pantalla
        const ui = new UI();

        // Input User Validation
        if (name === '' || price === '' || year === '') {
             ui.showMessage('Please Insert data in all fields', 'danger');
        }

        // Save Product
        ui.addProduct(product);
        ui.showMessage('Product Added Successfully', 'success');
        ui.resetForm();

        e.preventDefault();
    });

document.getElementById('product-list')
    .addEventListener('click', function (e) {
        const ui = new UI();
        // con el target no aseguramos de capturar el click de delete
        ui.deleteProduct(e.target);
        e.preventDefault();
    });